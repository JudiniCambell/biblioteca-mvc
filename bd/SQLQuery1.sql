﻿use master;  
drop database biblioteca;
create database biblioteca; 
use  biblioteca; 

create table estadoUsuario(
 id_estadoUsuario int primary key identity(1,1) not null,
 estado varchar(25) not null
);

insert into estadoUsuario values('Activo');
insert into estadoUsuario values('InActivo');

create table tipoUsuario(
	id_tipoUsuario int primary key identity(1,1)not null,
	tipo varchar(25)
);

insert into tipoUsuario values('Administrador');
insert into tipoUsuario values('Lector');

create table usuario(
	 id_usuario int primary key identity(1,1) not null,
	 documento bigint not null,
	 nombre varchar(250) not null,
	 celular bigint not null,
	 fotografia text null,
	 contrasena varchar(250) not null,
	 correo varchar(250)not null,
	 fk_estadoUsuario int not null,
	 fk_tipoUsuario int not null,
  foreign key (fk_estadoUsuario) references estadoUsuario(id_estadoUsuario),
  foreign key (fk_tipoUsuario) references tipoUsuario(id_tipoUsuario)
);

insert into usuario values('93201628','Carlos Jose Lopez Medina','3194665224','','123','admin@biblioteca.com',1,1);
insert into usuario values('93201628','Juan Andres Lopez ALvarez','3194665224','','123','jalopez1733@gmail.com',1,2);



create table editorial(
   id_editorial int primary key identity(1,1)not null,
   nombre varchar(250)not null,
   telefono varchar(250)null,
   correo varchar(250)null,
   dirrecion varchar(250) null
);

create table generoLiterario(
  id_genero int primary key identity(1,1) not null,
  nombre varchar(250),
  descripcion text not null
);


create table autor(
	id_autor int primary key identity(1,1) not null,
	nombre varchar(250) not null,
	fecha_nacimiento date not null,
	descripcion text not null,
	estado int not null
);

 
create table listaDeseos(
	   id_listaDeseos int primary key identity(1,1) not null,
	   nombreLibro varchar(250) not null,
	   valor varchar(250) not null,
	   recomendador varchar(250),
	   observacion text null,
	   fk_autor int not null,
	   fk_lector int not null,
   foreign key (fk_autor) references autor(id_autor),
   foreign key (fk_lector) references usuario(id_usuario)
);

create table estado_libro(
	id_estado int primary key identity(1,1) not null,
	estado varchar(250) not null
);


create table libro(
	 id_libro int primary key identity(1,1) not null,
	 nombre varchar(250) not null,
	 portada text null,
     fecha_adquirido date not null,
	 observacion text not null,
	 fk_estado_libro int not null,
	 fk_editorial int not null,
	 fk_autor int not null,
    foreign key (fk_editorial) references editorial(id_editorial),
    foreign key (fk_autor) references autor(id_autor),
	foreign key (fk_estado_libro ) references estado_libro(id_estado),
);

create table genero_libro(
	  id_genero_libro int primary key identity(1,1) not null,
	  fk_genero int not null,
	  fk_libro int not null,
  foreign key (fk_genero) references generoLiterario(id_genero),
  foreign key (fk_libro) references libro(id_libro)
);

create table prestamo(
	 id_prestamo int primary key identity(1,1) not null,
	 fk_usuario int not null,
	 fk_libro int not null,
	 estado varchar(25) not null,
	 fecha_prestamo datetime not null,
	 fecha_devolucion datetime not null,
	 observacion text null,
 foreign key (fk_usuario) references  usuario(id_usuario),
 foreign key (fk_libro) references libro(id_libro)
);

insert into estado_libro values('Activo');
insert into estado_libro values('Prestado');
insert into estado_libro values('Mantenimiento');
insert into estado_libro values('Desactivado');


insert into autor values('Edgar Allan Poe','1809-01-16','Edgar Allan Poe fue un escritor, poeta, crítico y periodista romántico​​ estadounidense, generalmente reconocido como uno de los maestros universales del relato corto.');
insert into autor values('H.P Lovecraft','1890-08-20','Fue un escritor estadounidense, autor de novelas y relatos de terror y ciencia ficción. Se le considera un gran innovador del cuento de terror.');
 
insert into  editorial values('Editorial Atenea ltda','475 5353 - 702 7087 - 313 865 8505','editorialatenea@hotmail.com','Carrera 64 #4 – 23');

insert into generoLiterario values('Cuento','El cuento es un género literario que ha gustado siempre a lectores y lectoras de todas las edades. Su forma es la de un relato breve que puede ocupar desde una sola página hasta algunas decenas. Se diferencia de la novela por su extensión.');
insert into generoLiterario values('Horror','El horror o terror es un género literario que se define por la sensación que causa: miedo. Nöel Carroll en su libro The Philosophy of Horror explica que la característica más importante del género horror es el efecto del que se causa en la audiencia, el horror necesariamente debe provocar miedo en el espectador.');

insert into libro values('Necronomicon 1 Trilogia del horror','','2019-07-14','Este libro fue comprado en  2019 nuevo en buen estado',1,1,1);
insert into genero_libro values(1,1);
insert into genero_libro values(2,1);



select * from usuario;
select * from editorial;
select * from autor;
select * from libro;
select * from genero_libro;
select * from generoLiterario;

  
