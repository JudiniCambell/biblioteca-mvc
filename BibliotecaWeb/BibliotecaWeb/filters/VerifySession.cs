﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BibliotecaWeb.Models;
using BibliotecaWeb.Controllers;

namespace BibliotecaWeb.filters
{
    public class VerifySession : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var bd = new bibliotecaEntities1();
            var oUser = (usuario)HttpContext.Current.Session["userActivo"];
            if (oUser ==null) {
                if (filterContext.Controller is AccessController == false){

                    filterContext.HttpContext.Response.Redirect("~/Access/Ingresar");
                }
            }

            base.OnActionExecuting(filterContext);
        }
    }
}