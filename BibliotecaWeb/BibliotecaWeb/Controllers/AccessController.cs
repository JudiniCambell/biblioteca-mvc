﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BibliotecaWeb.Models;
using BibliotecaWeb.Models.ViewModels;

namespace BibliotecaWeb.Controllers
{
    public class AccessController : Controller
    {
        [HttpGet]
        public ActionResult Ingresar() {
            return View();
        }

        [HttpPost]
        public ActionResult Ingresar(UserViewModel model) {

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            using (var db = new bibliotecaEntities1())
            {
                var list = from t1 in db.usuario
                           where t1.correo == model.Correo && t1.contrasena == model.Contrasena && t1.fk_estadoUsuario == 1
                           select t1;
                if (list.Count() == 1)
                {

                    Session["userActivo"] = list.First();
                    foreach (var dataUsuario in list)
                    {
                        
                        Session["userActivoTipo"] = dataUsuario.fk_tipoUsuario;
                        Session["userActivoNombre"] = dataUsuario.nombre;
                        if (dataUsuario.fk_tipoUsuario == 2)
                        {
                            return Redirect(Url.Content("~/Lector/Index"));
                        }
                        else if (dataUsuario.fk_tipoUsuario == 1)
                        {
                            return Redirect(Url.Content("~/Administrador/Index"));
                        }
                    }
                } 
                else
                {
                    model.estado = true;
                }
            }

            return View(model);

        }

        public ActionResult Logout()
        {
            Session["userActivo"] = null;
            return Redirect(Url.Content("~/Access/Ingresar"));
        }

    }
}