﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BibliotecaWeb.Models;
using BibliotecaWeb.Models.ViewModels;
using BibliotecaWeb.Models.TableViewModels;
using System.IO;

namespace BibliotecaWeb.Controllers
{
    public class LibrosController : Controller
    {
     
        [HttpGet]
        public ActionResult Consult() {
            int tipoS = ((int)Session["userActivoTipo"]);
            List<LibroTableViewModel> lista = null; 
            using (var db  = new bibliotecaEntities1()) {
                lista = (from t1 in db.libro
                         join t2 in db.autor on t1.fk_autor equals t2.id_autor
                         join t3 in db.editorial on t1.fk_editorial equals t3.id_editorial
                         orderby t1.nombre descending
                         select new LibroTableViewModel
                         {
                             Fk_estado_libro = t1.fk_estado_libro,
                             Id_libro = t1.id_libro,
                             Nombre = t1.nombre,
                             Portada = t1.portada,
                             Observacion = t1.observacion,
                             Autor = t2.nombre,
                             Editorial = t3.nombre,
                             generosLiterarios = (from t4 in db.generoLiterario
                                                  join t5 in db.genero_libro on t4.id_genero equals t5.fk_genero
                                                  where t5.fk_libro == t1.id_libro
                                                  orderby t4.nombre descending
                                                  select t4.nombre ).ToList()

                         }).ToList(); 
            } 
           return View(lista);
        }

        [HttpGet]
        public ActionResult Add() {


            List<EditorialTableViewModel> lEditorial = null;
            List<AutorTableViewModel> lAutor = null;
            List<GeneroLiterarioTableViewModel> lGenero = null;

            using (var db = new bibliotecaEntities1()) {
                lEditorial = (from t1 in db.editorial
                              orderby t1.nombre descending
                              select new EditorialTableViewModel
                              {
                                  Id_editorial = t1.id_editorial,
                                  Nombre = t1.nombre
                              }).ToList();

                lGenero = (from t1 in db.generoLiterario
                           orderby t1.nombre descending
                           select new GeneroLiterarioTableViewModel
                           {
                               Id_Genero = t1.id_genero,
                               Nombre = t1.nombre
                           }).ToList();

                lAutor = (from t1 in db.autor
                          orderby t1.nombre descending
                          select new AutorTableViewModel
                          {
                               Id_autor  = t1.id_autor,
                               Nombre= t1.nombre 
                          }).ToList();
            }

            ViewBag.vGenero = lGenero;
            ViewBag.vEditorial = lEditorial;
            ViewBag.vAutor = lAutor;
                return View();
        }

        [HttpPost]
        public ActionResult Add(LibroViewModel model,HttpPostedFileBase portada,int[] _generoLiterario,string _Autor,string _Editorial ){
            if (!ModelState.IsValid) {
                this.Add();
            }

 
            if (portada != null && portada.ContentLength>0)
            {
                try
                {

                    string nombreFinal = model.Nombre + "-" + DateTime.Now.ToString("MM-dd-yyyy") + Path.GetExtension(portada.FileName);
                    string rutaB= Path.Combine(Server.MapPath("~/Img/Portadas"),nombreFinal );
                    portada.SaveAs(rutaB); 
                    using (var db = new bibliotecaEntities1())
                    {
                        libro oLibro = new libro
                        {
                            nombre = model.Nombre,
                            portada = nombreFinal,
                            fecha_adquirido = DateTime.Parse(model.FechaAdquirido),
                            observacion = model.Observacion,
                            fk_estado_libro =1,
                            fk_editorial =  int.Parse(_Editorial) ,
                            fk_autor = int.Parse(_Autor)
                        };

                        db.libro.Add(oLibro);
                        db.SaveChanges();

                        for (int i = 0; i < _generoLiterario.Length; i++) {
                            genero_libro oGenero = new genero_libro
                            {
                                fk_libro = oLibro.id_libro,
                                fk_genero = _generoLiterario[i]
                            };
                            db.genero_libro.Add(oGenero);
                            db.SaveChanges();
                        }
                    }
                }
                catch (Exception)
                {
                    ViewBag.Message = "Error";
                }
            }

 

            return Redirect(Url.Content("~/Libros/Consult"));
        }
    }
}