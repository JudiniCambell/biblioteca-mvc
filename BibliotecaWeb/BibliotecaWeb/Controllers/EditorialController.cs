﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BibliotecaWeb.Models;
using BibliotecaWeb.Models.ViewModels;
using BibliotecaWeb.Models.TableViewModels;

namespace BibliotecaWeb.Controllers
{
    public class EditorialController : Controller
    {

        [HttpGet]
        public ActionResult Consult() {
            List<EditorialTableViewModel> lista = null; 
            using (var bd = new bibliotecaEntities1()) { 
                lista = (from t1 in bd.editorial
                          orderby t1.nombre descending
                          select new EditorialTableViewModel
                          {
                              Id_editorial = t1.id_editorial,
                              Nombre = t1.nombre,
                              Telefono = t1.telefono,
                              Correo = t1.correo, 
                              Dirrecion  =t1.dirrecion 
                          }).ToList(); 
            } 
          return View(lista);
        }

        [HttpGet]
        public ActionResult Add() {
            return View();
        }
        [HttpPost]
        public ActionResult Add(EditorialViewModel model) {

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            using (var bd = new bibliotecaEntities1())
            { 
                editorial oEditorial = new editorial
                { 
                    nombre = model.Nombre,
                    telefono = model.Telefono,
                    correo = model.Correo,
                    dirrecion = model.Correo
                };
                bd.editorial.Add(oEditorial);
                bd.SaveChanges();
            } 
                return Redirect(Url.Content("~/Editorial/Consult"));
        }

        [HttpPost]
        public ActionResult Edit(int nEditorial,string nombre,string telefono,string correo,string dirrecion) {
            using (var db = new bibliotecaEntities1()) {
                var oEditorial = db.editorial.Find(nEditorial);
                oEditorial.nombre = nombre;
                oEditorial.telefono = telefono;
                oEditorial.correo = correo;
                oEditorial.dirrecion = dirrecion;
                db.Entry(oEditorial).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            } 
                return Content("1");
        }

        [HttpDelete]
        public ActionResult Delete(int nEditorial) { 
            using (var db = new bibliotecaEntities1())
            {
               var editorial =  db.editorial.Find(nEditorial);
                db.Entry(editorial).State = System.Data.Entity.EntityState.Deleted;
                db.SaveChanges();
            } 
            return Content("1");
        }

        public ActionResult DaPrueba(JsonResult  data) { 
            Console.WriteLine(data); 
            return View(data);
        }
    }
}