﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BibliotecaWeb.Models;
using BibliotecaWeb.Models.ViewModels;
using BibliotecaWeb.Models.TableViewModels;

namespace BibliotecaWeb.Controllers
{
    public class AutorController : Controller
    {


        [HttpGet]
        public ActionResult Autor()
        {

            List<AutorTableViewModel> lista = null;
            using (var db = new bibliotecaEntities1())
            {
                lista = (from t1 in db.autor
                         orderby t1.nombre descending
                         where t1.estado == 1
                         select new AutorTableViewModel
                         {
                             Id_autor = t1.id_autor,
                             Nombre = t1.nombre,
                             Fecha_nacimiento = t1.fecha_nacimiento,
                             Descripcion = t1.descripcion,
                             N_Libros = (from t3 in db.libro where t1.id_autor == t3.fk_autor select t3.id_libro).Count()
                         }).ToList();

                /*    IList<AutorTableViewModel> algo = db.autor.Where(w => w.estado.Equals(1)).OrderByDescending(o => o.nombre)
                                                              .Select(s => new AutorTableViewModel
                                                              {
                                                                  Estado = s.estado ?? 0,  
                                                                  Descripcion = s.descripcion,
                                                                  Fecha_nacimiento = s.fecha_nacimiento,
                                                                  Id_autor = s.id_autor,
                                                                  N_Libros = db.libro.Where(e => e.fk_autor.Equals(s.id_autor)).Count()
                                                              }).ToList();
                */


            }
            return View(lista);
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        public ActionResult Add(AutorViewModel model)
        {

            if (!ModelState.IsValid)
            {
                return View(model);
            }


            using (var bd = new bibliotecaEntities1())
            {
                autor oAutor = new autor
                {
                    nombre = model.Nombre,
                    fecha_nacimiento = model.FechaNacimiento,
                    descripcion = model.Descripcion,
                    estado = 1
                };

                bd.autor.Add(oAutor);
                bd.SaveChanges();
            }

            return Redirect(Url.Content("~/Autor/Autor"));
        }



        public ActionResult Edit(int autor, string nombre, string fechaNacimiento, string descripcion)
        {
            Console.WriteLine(nombre);
            using (var db = new bibliotecaEntities1())
            {
                var oAutor = db.autor.Find(autor);
                oAutor.nombre = nombre;
                oAutor.fecha_nacimiento = Convert.ToDateTime(fechaNacimiento);
                oAutor.descripcion = descripcion;
                db.Entry(oAutor).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            return Content("1");
        }

        public ActionResult Delete(int autor)
        {
            using (var db = new bibliotecaEntities1())
            {
                var oAutor = db.autor.Find(autor);
                oAutor.estado = 2;
                db.Entry(oAutor).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            return Content("1");
        }
    }



}