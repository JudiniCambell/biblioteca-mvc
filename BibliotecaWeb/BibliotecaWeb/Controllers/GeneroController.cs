﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BibliotecaWeb.Models;
using BibliotecaWeb.Models.ViewModels;
using BibliotecaWeb.Models.TableViewModels; 

namespace BibliotecaWeb.Controllers
{
    public class GeneroController : Controller
    {
        [HttpGet]
        public ActionResult ConsultGenero() {
            List<GeneroLiterarioTableViewModel> lista = null;
            using (var db = new bibliotecaEntities1()) {
                lista = (from t1 in db.generoLiterario
                         orderby t1.nombre descending
                         select new GeneroLiterarioTableViewModel
                         {
                             Id_Genero = t1.id_genero,
                             Nombre = t1.nombre,
                             Descripcion = t1.descripcion
                         }).ToList();
            }
            return View(lista);
        }

        [HttpGet]
        public ActionResult Add() {
            return View();
        }

        [HttpPost]
        public ActionResult Add(GeneroLiterarioViewModel model) { 
            if (! ModelState.IsValid) {
                return View(model);
            }

            using (var db = new bibliotecaEntities1()) {

                generoLiterario oGenero = new generoLiterario
                {
                    nombre = model.Nombre,
                    descripcion = model.Descripcion
                }; 
                db.generoLiterario.Add(oGenero);
                db.SaveChanges();
            }
            return Redirect(Url.Content("~/Genero/ConsultGenero"));
        }

        public ActionResult Update(int NGenero, string nombre, string descripcion) {
             using (var db = new bibliotecaEntities1())
            {
                generoLiterario oGenero = db.generoLiterario.Find(NGenero);
                oGenero.nombre = nombre;
                oGenero.descripcion = descripcion;
                db.Entry(oGenero).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
                return Content("1");
        }
        public ActionResult Delete(int NGenero) { 
            using (var db = new bibliotecaEntities1()) {
                generoLiterario oGenero = db.generoLiterario.Find(NGenero); 
                db.Entry(oGenero).State = System.Data.Entity.EntityState.Deleted;
                db.SaveChanges();
            }

                return Content("1");
        }

    }
}