﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace BibliotecaWeb.Models.ViewModels
{
    public class GeneroLiterarioViewModel
    {
        [Required]
        [MaxLength(250,ErrorMessage ="El nombre debe ser menor a {0}")]
        [MinLength(1,ErrorMessage ="El nombre debe seer mayou a {0}")]
        [DataType(DataType.Text)]
        public string Nombre { get; set; }
        [Required]
        [MaxLength(1000, ErrorMessage = "El nombre debe ser menor a {0}")]
        [MinLength(1, ErrorMessage = "El nombre debe seer mayou a {0}")]
        [DataType(DataType.Text)]
        public string Descripcion { get; set; }
    }
}