﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations; 
using System.Web;



namespace BibliotecaWeb.Models.ViewModels
{
    public class UserViewModel
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Correo electronico")]
        [MaxLength(250,ErrorMessage ="El Campo debe tener como maximo {0} y minimo {1}"),MinLength(1)]
        public string Correo { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Contrasena")]
        public string Contrasena { get; set; }

        public bool estado { get; set; }
    }
}