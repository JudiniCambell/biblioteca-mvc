﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
namespace BibliotecaWeb.Models.ViewModels
{
    public class AutorViewModel
    {
        [Required]
        [Display(Name = "Nombre del autor")]
        [MaxLength(250, ErrorMessage = "El nombre del autor no debe de superar {0} y debe ser mayor a {1} "), MinLength(3)]
        public string Nombre { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [Display(Name = "Fecha de nacimiento")]
        public DateTime FechaNacimiento { get; set; }

        [Required]
        [Display(Name ="Descripcion")]
        [MaxLength(250, ErrorMessage = "La descripcion no debe superar a {0}  y debe de ser mayor a {1}"), MinLength(3)]
        public string Descripcion { get; set; }
    }
}