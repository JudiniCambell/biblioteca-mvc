﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace BibliotecaWeb.Models.ViewModels
{
    public class EditorialViewModel
    {
        [Required]
        [Display(Name ="Nombre")] 
        [MaxLength(250,ErrorMessage ="EL campo no debe ser mayor a {0}")]
        [MinLength(1,ErrorMessage ="El campo no debe ser menor de {0}")]
       public string Nombre { get; set; }

        [Required]
        [Display(Name = "Telefono")]
        [MaxLength(250, ErrorMessage = "EL campo no debe ser mayor a {0}")]
        [MinLength(1, ErrorMessage = "El campo no debe ser menor de {0}")]
        public string Telefono { get; set; }
        [Required]
        [Display(Name = "Correo")]
        [MaxLength(250, ErrorMessage = "EL campo no debe ser mayor a {0}")]
        [MinLength(1, ErrorMessage = "El campo no debe ser menor de {0}")]
        [DataType(DataType.EmailAddress)]
        public string Correo { get; set; }
        [Required]
        [Display(Name = "Dirrecion")]
        [MaxLength(250, ErrorMessage = "EL campo no debe ser mayor a {0}")]
        [MinLength(1, ErrorMessage = "El campo no debe ser menor de {0}")]
        public string Dirrecion { get; set; }
    }
}