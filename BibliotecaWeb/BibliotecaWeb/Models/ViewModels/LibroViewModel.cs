﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BibliotecaWeb.Models.ViewModels
{
    public class LibroViewModel
    {
        [Required]
        [Display(Name = "Nombre")]
        [MaxLength(250, ErrorMessage = "El campo debe tener como maximo {0}")]
        [MinLength(1, ErrorMessage = "El campo debe tener como minimo {0}")]
        [DataType(DataType.Text)]
        public string Nombre { get; set; }

        [Required]
        [Display(Name ="Portada")] 
        [DataType(DataType.Upload)]
        public string Portada { get; set; }

        [Required]
        [Display(Name = "Fecha de adquisicion")]
        [MaxLength(250, ErrorMessage = "El campo debe tener como maximo {0}")]
        [MinLength(1, ErrorMessage = "El campo debe tener como minimo {0}")]
        public string FechaAdquirido{ get; set; }

 


        [Required]
        [Display(Name = "Observacion")]
        [MaxLength(250, ErrorMessage = "El campo debe tener como maximo {0}")]
        [MinLength(1, ErrorMessage = "El campo debe tener como minimo {0}")]
        public string Observacion { get; set; } 
    }
}