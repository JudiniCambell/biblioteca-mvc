﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BibliotecaWeb.Models.TableViewModels
{
    public class GeneroLiterarioTableViewModel
    {
        public int Id_Genero { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        
    }

}