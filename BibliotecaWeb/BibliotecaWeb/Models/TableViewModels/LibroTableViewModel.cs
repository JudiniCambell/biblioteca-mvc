﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BibliotecaWeb.Models.TableViewModels
{
    public class LibroTableViewModel
    {
        public int Id_libro { get; set; }
        public string Nombre { get; set; }
        public string Portada { get; set; }
        public int Fk_editorial { get; set; }
        public int Fk_autor { get; set; }
        public int Fk_estado_libro { get; set; }
        public string Observacion { get; set; }

        public  List<string>  generosLiterarios { get; set; }


        public string Editorial { get; set; }
        public string Autor { get; set; }

    }
}