﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BibliotecaWeb.Models.TableViewModels
{
    public class AutorTableViewModel
    {

        public int Id_autor { get; set; }
        public string Nombre { get; set; }
        public DateTime Fecha_nacimiento { get; set; }

        public string Descripcion { get; set; }

        public int Estado { get; set; }
        public int N_Libros { get; set; }
    }
}